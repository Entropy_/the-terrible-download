This is a combination simple blogging platform/"mail to" blog.
It is written in go, with a little bash sprinkled in.

Images are not supported, however the colour of the page is
randomized and changes with every load. I have a fix that allows
for serving absolutely anything, however it requires a static
link to serve from. I would like to make a default way to 
serve media in general, so until that comes to fruition,
images are not supported.

Some set up is required, and as it opens a mailbox to the
forces of the internet, it is not recommended to install
locally.

If you're like me, and like bleeding edge software, you might
have paid for email from protonmail. Maybe you have some vanity
domain names you've turned into email addresses, whatever
your reason for using protonmail, I have a setup with a headless
server that works with the linux bridge.

***These instructions are for Fedora 29***


***THIS REQUIRES YOU TO TYPE YOUR PASSWORD IN PLAINTEXT OVER SSH***

If you're aware of the risks, let's dive in!


First off, you need to install it under the home folder
of a user named markup and get all dependencies needed.

`adduser -m markup`

This will add a user with the required name, and create
a home directory for the user.

Install protonmail-bridge. As it is a beta package, and
requirements for copy-pasting the commands change with
time, the next lines include variables you should change
and enter the current package name and package HTML with.

`wget $PROTONMAIL-BRIDGE-HTML-LOCATION`

`sudo dnf install gnome-python2-gnomekeyring nano getmail git golang wget gpg gnome-keyring pass tmux @gnome-desktop cronie $PROTONMAIL-BRIDGE-PACKAGE-NAME`

This is a good time to read a book, walk to the store
or eat dinner. It'll take a bit.

Once that's all installed, we need the system to process the changes.

`shutdown -r now`

Then ssh back in to your server and issue the following

`python2 -c "import gnomekeyring;gnomekeyring.unlock_sync(None, 'YOUR-LOGIN-PASSWORD');"`

`protonmail-bridge --cli`

`login`

<<login normally here, prefereably with 2FA>>

If you have more than one protonmail address, make
sure you do the following.

`change mode`

Answer yes to the prompt and it will split your setup into 
seperate inboxes for seperate emails.

`info`

This command in bridge will give you your bridge-specific password. Write
it down or paste it somewhere, because we're about to daemonize
it so it will run in the backgound.

Now you can leave the interactive bridge!

`exit`

Now spawn a session that runs the bridge in the background
for this login.

`tmux new-session -d -s mail 'protonmail-bridge --cli'`

To make this happen every time you login, so your
blog is active and receiving emails once you ssh in,
edit the `tools/startbridge.sh` file to have your
login password, then run the following to add it
to your .bash_profile and make it load everytime you ssh
in.

`mkdir -p /home/root/`

`sudo cat /home/go/src/gitlab.com/py-entro/the-terrible-download/tools/startbridge.sh >> /home/root/.bash_profile`

The @gnome-desktop adds a whole lot of cruft I wish weren't
needed for a headless install. If you know the workaround
for fedora and protonmail-bridge, let me know. This is
the only solution I've come up with yet.(it relies on dbus
being active, but dbus only activates in the presence of
an X11 system like @gnome-desktop)

Now that the hard work is done, it's time to set up our
system!

markup should be in the sudoers file if we want to be
able to run administrative concerns with it.

`gpasswd wheel -a markup`

Now we should switch to the user markup to make sure
everything we create from now on is owned by the right user.

`su markup`

The next is to create the directory structure that will
house your install.

`mkdir -p /home/markup/go/src/gitlab.com/py-entro/`

`cd /home/markup/go/src/gitlab.com/py-entro`

Then we get the source code.

`git clone https://gitlab.com/py-entro/the-terrible-download.git`

Now we need something to receive emails, I use getmail and we need
an rc file for getmail to reference while getting the mail.

What follows is an example of a config file for getmail and 
protonmail bridge.
`touch mailrc`

`[retriever]`

`type = SimpleIMAPRetriever`

`server = BRIDGE-SERVER`

`port = BRIDGE-PORT`

`username = YOUR-BRIDGE-USERNAME`

`password = YOUR-BRIDGE-PASSWORD`



`[destination]`

`type = Maildir`

`path = /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/dl`

place this file in `the-terrible-download/dl/mailrc`

Now we need to schedule the task to be run, which
is getting mail. We're going to use crontab, or 
cron table, which is a system level task scheduler
for linux systems.

`crontab -e`

Opens up the crontab. A window will open with a default
editor, remember, if you're not comfortable with Vi,
two commands to write and quit are as follows.
`:w`

`:q`
If you get stuck in insert only(writing text) mode, 
pressing escape, and then typing your commands should work.

The line we are going to add to our crontab file is
as follows

`14 * * * * getmail -r mailrc --getmaildir /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/dl/`

The first chunk shows how often the commmand is to be 
run, and goes as follows, every 14th minute, of every
hour, of every day, of every month, every day of the week.
The next section is the getmail command we're running.

Now, we have one other thing to run as a cron job
and it's the mail sorter, and the reason we used 
every 14th minute instead of every 15th. Every 15th
minute we want to sort though all the things we
received and post one of them to the front page.

so on the next line of our crontab, put the following line

`15 * * * * /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/tools/mailsort.sh`

Mailsort.sh actually stores the current password as well.

Open mailsort.sh in the editor of your choice,
and change the line that contains
`PASS="Pass: porkchop"`
Put whatever you like in place of porkchop. While
delicious, they have served their purpose.

Next, we're going to start the actual server!

Well, get a config in really.

change directory into the tools folder

`cd /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/tools`

and copy the config file to a system folder that will
allow it to be automatically restarted on shutdown.
As an added bonus, it lets us start it with a simple
command, rather than going through all of this over again.

`sudo cp the.terrible.download.config /etc/systemd/system/the.terrible.download.service`

Now we have to tell the system that we added something
it should know about.

`systemctl enable the.terrible.download.service`

You will be asked to authenticate a few times for this.
If you've made it this far, you're probably going
to say yes.

Protonmail runs encryption on basically everything,
so why not give them a tip of the hat and enable
HTTPS on our blog?

Let's encrypt!

With Let's Encrypt's certbot!

`dnf install certbot`

Now, just run certbot with the following command. Warning,
you must have this domain currently pointing to your
server. Certbot pops a message from the current location
and does its magic from there.

`sudo certbot certonly --standalone`

Now we have to install the fullchain.pem and privkey.pem
The only line you have to change in the source code is the
very last line of code. Change the.terrible.download to your
own domain name of choice.

an example of where to put the files on a fedora system

`/etc/letsencrypt/live/the.terrible.download/fullchain.pem`

`/etc/letsencrypt/live/the.terrible.download/privkey.pem`

There is another problem, and that is that the keys are owned
by root! We need to change that by making a group called markup
and adding markup and root to it. Then make the files owned
by the markup group, and allowing them to make changes to said files.

`sudo usermod -a -G wheel markup`

`sudo usermod -a -G wheel,markup root`

`sudo chgrp markup /etc/letsencrypt/live`

`sudo chgrp markup /etc/letsencrypt/archive`

`sudo chmod 710 /etc/letsencrypt/live`

`sudo chmod 710 /etc/letsencrypt/archive`

Now we need to let the executable have access to lower level ports!

First let's build the binary.

`cd /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/`

`go build`

For the go build command, no news is good news. If you encounter
any bugs or errors at this step, send me an email at hakphar@snowcrash.network
or open an issue on this repo!

Next, allow the binary to have access

`sudo setcap CAP_NET_BIND_SERVICE=+eip /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/the-terrible-download`

And we have to apply the selinux rules to allow the server
process access to the files. Run the following commands.

`semodule -i /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/selinux/download.pp`

`semodule -i /home/markup/go/src/gitlab.com/py-entro/the-terrible-download/selinux/the-terrible-do.pp`


Now, flip the switch and your new website will appear at your hosted domain!

`systemctl start the.terrible.download.service`

Corngrabulations! You now have a working "Mail-to" blog!

Now, we need to actually know how to get mail sent to the blog
to process properly!

It's simple, if an email contains the contents of `tools/exampleemail.txt`
in the proper order, it will trigger on the most recent email matching those parameters.
Protonmail requires the Content-Type header for sending emails from emacs, I'm guessing
it'll be the same no matter what you use. The `Pass: porkchop` will get stripped if you
use it as a header, so just put that at the top of the body of your email and it'll work fine.

**important**
The blog doesn't directly read that file, you have to change the values
in the `/tools/mailsort.sh` script. If you're impatient like me, and want to force
the system to update, run `tools/getmail.sh` followed by `tools/mailsort.sh`
and reload your page.

And that's it! Thanks for tuning in, and if you want to tip, there's a link
in the bottom corner of the blog. Feel free to point it wherever you like
by editing the values in the `the-terrible-download/www` folder.

Entropy
