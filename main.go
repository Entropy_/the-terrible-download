package main

import (
	"strings"
	"crypto/tls"
	"log"
	"fmt"
	"math/rand"
	"net/http"
	"errors"
	"io/ioutil"
	"bufio"
	"github.com/gin-gonic/gin"
	"time"
	"os"
)

func chooseColor() string {
colors := []string {
"Pink",
"LightPink",
"HotPink",
"DeepPink",
"PaleVioletRed",
"MediumVioletRed",
"LightSalmon",
"Salmon",
"DarkSalmon",
"LightCoral",
"Crimson",
"FireBrick",
"DarkRed",
"Red",
"OrangeRed",
"Tomato",
"Coral",
"DarkOrange",
"Orange",
"Yellow",
"LightYellow",
"LemonChiffon",
"LightGoldenrodYellow",
"PapayaWhip",
"Moccasin",

"PeachPuff",

"PaleGoldenrod",

"Khaki",

"DarkKhaki",

"Gold",

"Cornsilk",

"BlanchedAlmond",

"Bisque",

"NavajoWhite",

"Wheat",

"BurlyWood",

"Tan",

"RosyBrown",

"SandyBrown",

"Goldenrod",

"DarkGoldenrod",

"Peru",

"Chocolate",

"SaddleBrown",

"Sienna",

"Brown",

"Maroon",

"DarkOliveGreen",

"Olive",

"OliveDrab",

"YellowGreen",

"LimeGreen",

"Lime",

"LawnGreen",

"Chartreuse",

"GreenYellow",

"SpringGreen",

"MediumSpringGreen",

"LightGreen",

"PaleGreen",

"DarkSeaGreen",

"MediumAquamarine",

"MediumSeaGreen",

"SeaGreen",

"ForestGreen",

"Green",

"DarkGreen",

"Aqua",

"Cyan",

"LightCyan",

"PaleTurquoise",

"Aquamarine",

"Turquoise",

"MediumTurquoise",

"DarkTurquoise",

"LightSeaGreen",

"CadetBlue",

"DarkCyan",

"Teal",

"LightSteelBlue",

"PowderBlue",

"LightBlue",

"SkyBlue",

"LightSkyBlue",

"DeepSkyBlue",

"DodgerBlue",

"CornflowerBlue",

"SteelBlue",

"RoyalBlue",

"Blue",

"MediumBlue",

"DarkBlue",

"Navy",

"MidnightBlue",

"Lavender",

"Thistle",

"Plum",

"Violet",

"Orchid",

"Fuchsia",

"Magenta",

"MediumOrchid",

"MediumPurple",

"BlueViolet",

"DarkViolet",

"DarkOrchid",

"DarkMagentax",

"Purple",

"Indigo",

"DarkSlateBlue",

"SlateBlue",

"MediumSlateBlue",

"White",

"Snow",

"Honeydew",

"MintCream",

"Azure",

"AliceBlue",

"GhostWhite",

"WhiteSmoke",

"Seashell",

"Beige",

"OldLace",

"FloralWhite",

"Ivory",

"AntiqueWhite",

"Linen",

"LavenderBlush",

"MistyRose",

"Gainsboro",

"LightGray",

"Silver",

"DarkGray",

"Gray",

"DimGray",

"LightSlateGray",

"SlateGray",

"DarkSlateGray",

"Black",
}

	color := rand.Intn(len(colors)-1)
	return colors[color]
}


func RecoverWrap(h http.HandlerFunc) http.HandlerFunc {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        var err error
        defer func() {
            r := recover()
            if r != nil {
                switch t := r.(type) {
                case string:
                    err = errors.New(t)
                case error:
                    err = t
                default:
                    err = errors.New("Unknown error")
                }
                http.Error(w, err.Error(), http.StatusInternalServerError)
            }
        }()
        h.ServeHTTP(w, r)
    })
}
type PremiumContent struct{
	Data	map[int][]byte
	Content map[int][]byte
}

var morph bool

func createWebsite(path string) PremiumContent{
	user := os.Args[1]

	prefix := "/home/"+string(user)+"/go/src/gitlab.com/entro-pi/the-terrible-download/"
	files, err := ioutil.ReadDir(prefix + "downloads/")
	if err != nil {
		fmt.Println("Error reading downloads folder")
	}
	var content PremiumContent
	for _, contentTemp := range files {
		file, err := os.Open(prefix + "downloads/" + contentTemp.Name())
		tempData, err := ioutil.ReadAll(file)
		data := make(map[int][]byte)
		data[0] = tempData
		if err != nil {
			fmt.Println("Error getting file in createWebsite")
		}
		content.Data = data
	}

	contentHolder := make(map[int][]byte)
	contentHolder[0] = content.Data[0]
	content.Content = contentHolder
	return content

}

func callback(w http.ResponseWriter, req *http.Request) {
	time.Sleep(5*time.Second)
	w.Write([]byte("THE CAKE IS A LIE"))

}

var count int

func obtained(c *gin.Context) {
	
	user := os.Args[1]
	prefix := "/home/"+string(user)+"/go/src/gitlab.com/entro-pi/the-terrible-download/"
	 
		if c.Request.URL.Path == "/robots.txt" {
			//countBot++
			robots, err := os.Open(prefix + "robots.txt")
			defer robots.Close()
			if err != nil {
				fmt.Println("Where'd the dang robots go?")
			}
			robotData, err := ioutil.ReadAll(robots)
			if err != nil {
				fmt.Println("Still can't find those dang robots")
			}
			c.Writer.Write(robotData)
			//fmt.Println("Disallowed a robot from", c.UserAgent(), ", this is the", countBot, "time we've had to do this.")
		}


		// Get the headers and footers

		headerData, err := os.Open(prefix + "www/header01")
		headerData02, err := os.Open(prefix + "www/header02")
		defer headerData.Close()
		defer headerData02.Close()
		footerData, err := os.Open(prefix + "www/footer01")
		footerData02, err := os.Open(prefix + "www/footer02")
		defer footerData.Close()
		defer footerData02.Close()
		if err != nil {
		        fmt.Println("Error getting header/footer")
		}

		// Get the headers and footers

		header, err := ioutil.ReadAll(headerData)
		header02, err := ioutil.ReadAll(headerData02)
		footer, err := ioutil.ReadAll(footerData)
		footer02, err := ioutil.ReadAll(footerData02)
		if err != nil {
			fmt.Println("Error reading header and footer")
		}

		// Get the premium content

		fileReader, err := os.Open(prefix + "downloads/01")
		defer fileReader.Close()

		if err != nil {
		        fmt.Println("Error getting the download")
		}

		file := bufio.NewScanner(fileReader)



		// Get the morphling

		morphFile, err := os.Open(prefix + "www/morphling")
		defer morphFile.Close()
		if err != nil {
			fmt.Println("Error opening the morphling")
		}

		morphling, err := ioutil.ReadAll(morphFile)
		if err != nil {
			fmt.Println("Error reading the morphling")
		}

		// Until we get a proper morphling
		fmt.Println(string(morphling))
		// Until we get a proper morphling
		//newLine := ""
		//makeLine := false


		// Write the header, statusOK 200 and randomize the colour of the main column.
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write(header)
		color := chooseColor()
		c.Writer.Write([]byte(color))
		c.Writer.Write(header02)
		for file.Scan() {
			if (!strings.Contains(file.Text(), "--")) {

				c.Writer.Write([]byte(file.Text()))
				c.Writer.Write([]byte("<br>"))
			}
		}
		//text := ""
/*		for file.Scan() {
		        for i := range file.Text() {
		             text = file.Text()
			     if text == "" {
				makeLine = true
			     }
			     newLine +=  string(text[i])
		             if i % 40 == 0 {
		                   makeLine = true
		             }
			     if makeLine && string(newLine[len(newLine)-1]) == " "{
                               	w.Write([]byte(newLine))
                               	w.Write([]byte("<br>"))
                               	newLine = ""
                               	makeLine = false
                              }

		        }
		        if makeLine && string(newLine[len(newLine)-1]) == " "{
				w.Write([]byte(file.Text()))
				w.Write([]byte("<br>"))
				newLine = ""
				makeLine = false
			}
		}
*/
		// Write to footer
		c.Writer.Write(footer)
		c.Writer.Write([]byte(color))
		c.Writer.Write(footer02)
		count++
		fmt.Println("Served ", count, " terrible downloads.")
		morph = true
		// define our later call
		
	
	
}
func writeIn(path string) string {
		output := ""

		file, err := os.Open(path)
		if err != nil {
			panic(err)
		}
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			output += scanner.Text()
		}
		return output
}


func main() {
	user := os.Args[1]
	rand.Seed(int64(time.Now().Nanosecond()))
	prefix := "/home/"+string(user)+"/go/src/gitlab.com/entro-pi/the-terrible-download/"

	countBot := 0
	mux := http.NewServeMux()
	count := 0
	morph := false

	mux.HandleFunc("/#", RecoverWrap(func(w http.ResponseWriter, req *http.Request) {

		time.Sleep(5*time.Second)
		w.Write([]byte("boop"))

	}))
	
	mux.HandleFunc("/www", RecoverWrap(func(w http.ResponseWriter, req *http.Request){
			
	}))
	
	mux.HandleFunc("/speed", RecoverWrap(func(w http.ResponseWriter, req *http.Request) {
		//waaaah hamburgers!
		output := ""

		output += writeIn("www/header")
		output += writeIn("www/newstyle.css")
		output += writeIn("downloads/01.html")
		output += writeIn("www/footer")
		
		w.Write([]byte(output))



	}))
        mux.HandleFunc("/", RecoverWrap(func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path == "/robots.txt" {
			countBot++
			robots, err := os.Open(prefix + "robots.txt")
			defer robots.Close()
			if err != nil {
				fmt.Println("Where'd the dang robots go?")
			}
			robotData, err := ioutil.ReadAll(robots)
			if err != nil {
				fmt.Println("Still can't find those dang robots")
			}
			w.Write(robotData)
			fmt.Println("Disallowed a robot from", req.UserAgent(), ", this is the", countBot, "time we've had to do this.")
		}


		// Get the headers and footers

		headerData, err := os.Open(prefix + "www/header01")
		headerData02, err := os.Open(prefix + "www/header02")
		defer headerData.Close()
		defer headerData02.Close()
		footerData, err := os.Open(prefix + "www/footer01")
		footerData02, err := os.Open(prefix + "www/footer02")
		defer footerData.Close()
		defer footerData02.Close()
		if err != nil {
		        fmt.Println("Error getting header/footer")
		}

		// Get the headers and footers

		header, err := ioutil.ReadAll(headerData)
		header02, err := ioutil.ReadAll(headerData02)
		footer, err := ioutil.ReadAll(footerData)
		footer02, err := ioutil.ReadAll(footerData02)
		if err != nil {
			fmt.Println("Error reading header and footer")
		}

		// Get the premium content

		fileReader, err := os.Open(prefix + "downloads/01")
		defer fileReader.Close()

		if err != nil {
		        fmt.Println("Error getting the download")
		}

		file := bufio.NewScanner(fileReader)



		// Get the morphling

		morphFile, err := os.Open(prefix + "www/morphling")
		defer morphFile.Close()
		if err != nil {
			fmt.Println("Error opening the morphling")
		}

		morphling, err := ioutil.ReadAll(morphFile)
		if err != nil {
			fmt.Println("Error reading the morphling")
		}

		// Until we get a proper morphling
		fmt.Println(string(morphling))
		// Until we get a proper morphling
		//newLine := ""
		//makeLine := false


		// Write the header, statusOK 200 and randomize the colour of the main column.
		w.WriteHeader(http.StatusOK)
		w.Write(header)
		color := chooseColor()
		w.Write([]byte(color))
		w.Write(header02)
		for file.Scan() {
			if (!strings.Contains(file.Text(), "--")) {

				w.Write([]byte(file.Text()))
				w.Write([]byte("<br>"))
			}
		}
		//text := ""
/*		for file.Scan() {
		        for i := range file.Text() {
		             text = file.Text()
			     if text == "" {
				makeLine = true
			     }
			     newLine +=  string(text[i])
		             if i % 40 == 0 {
		                   makeLine = true
		             }
			     if makeLine && string(newLine[len(newLine)-1]) == " "{
                               	w.Write([]byte(newLine))
                               	w.Write([]byte("<br>"))
                               	newLine = ""
                               	makeLine = false
                              }

		        }
		        if makeLine && string(newLine[len(newLine)-1]) == " "{
				w.Write([]byte(file.Text()))
				w.Write([]byte("<br>"))
				newLine = ""
				makeLine = false
			}
		}
*/
		// Write to footer
		w.Write(footer)
		w.Write([]byte(color))
		w.Write(footer02)
		count++
		fmt.Println("Served ", count, " terrible downloads.")
		morph = true
		// define our later call
		
	}))
		server := gin.Default()

		server.Static("www", "/www")


        cfg := &tls.Config{
        MinVersion:               tls.VersionTLS12,
        CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
        PreferServerCipherSuites: true,
        CipherSuites: []uint16{
        	tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
                tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
                tls.TLS_RSA_WITH_AES_256_CBC_SHA,
                },
        }
		
        srv := &http.Server{
                Addr:         ":443",
                Handler:      mux,
                TLSConfig:    cfg,
                TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
		}
		server.NoRoute(obtained)
		
	for {
	        log.Fatal(srv.ListenAndServeTLS("/etc/letsencrypt/live/"+os.Args[2]+"/fullchain.pem", "/etc/letsencrypt/live/"+os.Args[2]+"/privkey.pem"))
	}

}
