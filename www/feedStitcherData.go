dev := terrible.download <- 02

Got the server all spun up. Found out Markdown is not the language that I want to do anything with, as it is a layout only language. I managed to do everything I wanted to do with inline CSS and HTML5. I might tweak the colours a bit more, make it more dark-theme-y. Also I want a good font.

The server was simple enough, I made it kinda tricky so it has the option of serving everything if need be. But I'm going to take those parts out. Or at least just update the server with a hard copy of the entry I want to show. As this is an exercise in owning everything I do online, that's what I'm going to do.

